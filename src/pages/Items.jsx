/**
 * Created by rodrigo on 07/05/17.
 */
import React, {Component} from "react";
import {Link, Route} from "react-router-dom";
import Item from "./Item";

export default class Items extends Component {

	isCurrentRoute = (route) => {
		return window.location.pathname.indexOf(route) !== -1 ? "active" : "";
	};

	render() {
		return (
			<div>
				<h1>Items page</h1>

				<Route exact path={this.props.match.url} render={() => (
					<h3>Welcome to Items Page. Please select a item.</h3>
				)}/>

				<ul className="nav nav-tabs">
					<li className={this.isCurrentRoute("1")}>
						<Link to={`${this.props.match.url}/1`}>Item 1</Link>
					</li>
					<li className={this.isCurrentRoute("2")}>
						<Link to={`${this.props.match.url}/2`}>Item 2</Link>
					</li>
				</ul>

				<div className="tab-content">
					<Route path={`${this.props.match.url}/:itemId`} component={Item}/>
				</div>
			</div>
		);
	}
}
